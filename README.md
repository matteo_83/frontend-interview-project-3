# Investoo Front-End (Vue) Technical Test
This technical test includes 2 small bugs that need to be fixed. We would like you to setup, start the app and fix the bugs.

The bugs:
- There's an error in the console, stopping the page from loading.
- The button isn't working.

If you fix these 2 bugs the app will reveal instructions on how to submit your final code as well as a small challenge that you can complete for bonus points.

We don't intend for this test to take you longer than 15 minutes, so please don't feel obliged to spend more time on this than that.

The code is in Vue, but neither bug are specifically Vue related. You may, however, find the [Vue documentation](https://vuejs.org/v2/guide/) useful in understanding the code.

## Project setup
```
npm install
```

### Serves the app for development
```
npm run serve
```
